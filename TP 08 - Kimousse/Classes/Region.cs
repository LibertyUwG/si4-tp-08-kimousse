﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_08___Kimousse.Classes
{
    class Region
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public int IdPays { get; set; }
    }
}
